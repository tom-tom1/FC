class Student:
    def __init__(self, first_name, last_name, class_name):
        self.first_name = first_name
        self.last_name = last_name
        self.class_name = class_name

class Teacher:
    def __init__(self, first_name, last_name, subject, classes_taught):
        self.first_name = first_name
        self.last_name = last_name
        self.subject = subject
        self.classes_taught = classes_taught

class HomeroomTeacher:
    def __init__(self, first_name, last_name, class_lead):
        self.first_name = first_name
        self.last_name = last_name
        self.class_lead = class_lead

users = []
commands = ["create", "manage", "end"]
user_types = ["student", "teacher", "homeroom teacher"]
manage_menu = ["class", "student", "teacher", "homeroom teacher", "end"]

while True:
    print("\nAvailable commands:")
    for x in commands:
        print("- " + x)
    print("Please enter the command:")
    input_command = input()
    if input_command == "create":
        print("\nUser types:")
        for x in user_types:
            print("- " + x)
        user_type = input("Enter the type of user to create: ")
        if user_type == "student":
            first_name = input("First name: ")
            last_name = input("Last name: ")
            class_name = input("Class name: ")
            users.append(Student(first_name, last_name, class_name))
        elif user_type == "teacher":
            first_name = input("First name: ")
            last_name = input("Last name: ")
            subject = input("Subject: ")
            classes_taught = []
            while True:
                class_taught = input("Class taught - Leave blank to end: ")
                if not class_taught:
                    break
                classes_taught.append(class_taught)
            users.append(Teacher(first_name, last_name, subject, classes_taught))
        elif user_type == "homeroom teacher":
            first_name = input("First name: ")
            last_name = input("Last name: ")
            class_lead = input("Class led: ")
            users.append(HomeroomTeacher(first_name, last_name, class_lead))
        else:
            print("Invalid user")
    elif input_command == "manage":
        while True:
            print("\nAvailable commands:")
            for x in manage_menu:
                print("- " + x)
            option = input("Enter an option to manage: ")
            if option == "class":
                class_name = input("Enter class name: ")
                print(f"\nStudents in class {class_name}:")
                for user in users:
                    if isinstance(user, Student) and user.class_name == class_name:
                        print(f"{user.first_name} {user.last_name}")
                print("\nHomeroom teacher:")
                for user in users:
                    if isinstance(user, HomeroomTeacher) and user.class_lead == class_name:
                        print(f"{user.first_name} {user.last_name}")
            elif option == "student":
                student_name = input("First name: ")
                student_surname = input("Last name: ")
                print("Student is a member of the following class:")
                for user in users:
                    if isinstance(user, Student) and user.first_name == student_name and user.last_name == student_surname:
                        class_name = user.class_name
                        print(f"{class_name}")
                print("Teacher of the class:")
                for user in users:
                    if (isinstance(user, HomeroomTeacher) and user.class_lead == class_name) or (isinstance(user, Teacher) and user.classes_taught == class_name):
                        print(f"{user.first_name} {user.last_name}")
            elif option == "teacher":
                teacher_name = input("First name: ")
                teacher_surname = input("Last name: ")
                for user in users:
                    if isinstance(user, Teacher) and user.first_name == teacher_name and user.last_name == teacher_surname:
                        print(f"\nTeacher {teacher_name} {teacher_surname} teaches:")
                        for class_taught in user.classes_taught:
                            print(class_taught)
            elif option == "homeroom teacher":
                teacher_name = input("First name: ")
                teacher_surname = input("Last name: ")
                for user in users:
                    if isinstance(user, HomeroomTeacher) and user.first_name == teacher_name and user.last_name == teacher_surname:
                        print(f"\nTeacher {teacher_name} {teacher_surname} teaches:")
                        class_members = user.class_lead
                        for user in users:
                            if isinstance(user, Student) and user.class_name == class_members:
                                print(f"{user.first_name} {user.last_name}")
            elif option == "end":
                break
            else:
                print("Invalid command")
    elif input_command =="end":
        print("Goodbye")
        break
    else:
        print("Invalid command")
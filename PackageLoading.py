packageNo = 1
packageWght = 0
totalWght = 0
unusedCap = 0
packageLimit = 20
mostUnused = 0
mostUnPckg = 1

itemNo = int(input("Enter maximum number of items in package\n"))
currentItem = 1

while itemNo != 0:
    print("Enter weight of item number {} ranging from 1 to 10".format(currentItem))
    tempWght = int(input())

    if tempWght >= 1 and tempWght <= 10:
        checkWght = tempWght + packageWght
        if checkWght > packageLimit:
            print("Added item would exceed 20kg package limit. Sending package number {} and starting new package".format(packageNo))
            unusedCap += packageLimit - packageWght
            totalWght += packageWght
            if mostUnused < (packageLimit - packageWght):
                mostUnused = packageLimit - packageWght
                mostUnPckg = packageNo
            packageWght = 0
            packageNo += 1
        packageWght += tempWght
        print(packageWght)
    elif tempWght == 0:
        print("0 inserted. Exiting")
        itemNo = 0
        break
    else:
        print("Incorrect weight provided. Range should be from 1 to 10")
        continue

    itemNo -= 1
    currentItem += 1

if  packageWght > 0:
    print("Last item reached. Sending remaining items")
    unusedCap += packageLimit - packageWght
    totalWght += packageWght
    if mostUnused < (packageLimit - packageWght):
        mostUnused = packageLimit - packageWght
        mostUnPckg = packageNo
    packageWght = 0
    packageNo += 1
    packageWght += tempWght
    print(packageWght)



packageNo -= 1
print("Packages sent: {}".format(packageNo))
print("Total weight: {}kg".format(totalWght))
print("Total unused capacity: {}kg".format(unusedCap))
print("Most unused capacity package number: {}, with {}kg of unsed capacity".format(mostUnPckg, mostUnused))
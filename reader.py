import csv
import os
import sys

def read_csv_file(filepath):
    with open(filepath, mode='r', newline='') as file:
        reader = csv.reader(file)
        data = [row for row in reader]
    return data

def write_csv_file(filepath, data):
    with open(filepath, mode='w', newline='') as file:
        writer = csv.writer(file)
        writer.writerows(data)

def apply_changes(data, changes):
    for change in changes:
        try:
            col, row, value = change.split(',')
            col, row = int(col), int(row)
            if row < len(data) and col < len(data[row]):
                data[row][col] = value
            else:
                print(f"Invalid change '{change}': row or column out of range")
        except ValueError:
            print(f"Invalid change format: '{change}' (expected format: X,Y,value)")
    return data

def display_csv(data):
    for row in data:
        print(','.join(row))

def main():
    if len(sys.argv) < 4:
        print("Usage: reader.py <src> <dst> <change1> <change2> ...")
        return

    src = sys.argv[1]
    dst = sys.argv[2]
    changes = sys.argv[3:]

    if not os.path.isfile(src):
        print(f"Error: Source file '{src}' does not exist or is not a file.")
        print("Files in the current directory:")
        for file in os.listdir('.'):
            print(file)
        return

    try:
        data = read_csv_file(src)
    except Exception as e:
        print(f"Error reading file '{src}': {e}")
        return

    data = apply_changes(data, changes)

    display_csv(data)

    try:
        write_csv_file(dst, data)
    except Exception as e:
        print(f"Error writing file '{dst}': {e}")
        return

if __name__ == "__main__":
    main()
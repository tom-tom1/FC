rec_name = ""
rec_year = 0
user_message = ""
user_name = ""
current_year = 2024
rec_age = 0

print("Please enter recipient's name:")
rec_name = input()
print("Please enter recipient's year of birth:")
rec_year = int(input())
print("Please enter your message:")
user_message = input()
print("Please enter your name:")
user_name = input()
rec_age = current_year - rec_year
print("{}, let's celebrate your {} years of awesomeness!".format(rec_name, rec_age))
print("Wishing you a day filled with joy and laughter as you turn {}!\n".format(rec_age))
print(user_message)
print("\nWith love and best wishes,")
print(user_name)
from flask import Flask, request, redirect, url_for, render_template, jsonify
import json
import os

app = Flask(__name__)

# Initialize data
account_balance = 0.0
stock_level = {}
history = []

# Load history from file
def load_history():
    global history
    if os.path.exists('history.json'):
        with open('history.json', 'r') as file:
            history.extend(json.load(file))

# Save history to file
def save_history():
    with open('history.json', 'w') as file:
        json.dump(history, file)

# Load initial data
load_history()

@app.route('/')
def index():
    return render_template('index.html', account_balance=account_balance, stock_level=stock_level)

@app.route('/purchase', methods=['POST'])
def purchase():
    product = request.form['purchase-product']
    price = float(request.form['purchase-price'])
    quantity = int(request.form['purchase-quantity'])
    
    global account_balance
    global stock_level

    total_cost = price * quantity

    if price < 0 or quantity < 1:
        return "Invalid input", 400

    if total_cost > account_balance:
        return "Insufficient funds for this purchase", 400

    account_balance -= total_cost
    if product in stock_level:
        stock_level[product] += quantity
    else:
        stock_level[product] = quantity

    history.append({'type': 'purchase', 'product': product, 'price': price, 'quantity': quantity})
    save_history()
    
    return redirect(url_for('index'))

@app.route('/sale', methods=['POST'])
def sale():
    product = request.form['sale-product']
    price = float(request.form['sale-price'])
    quantity = int(request.form['sale-quantity'])
    
    global account_balance
    global stock_level

    if price < 0 or quantity < 1 or product not in stock_level or stock_level[product] < quantity:
        return "Invalid input or insufficient stock", 400

    account_balance += price * quantity
    stock_level[product] -= quantity

    history.append({'type': 'sale', 'product': product, 'price': price, 'quantity': quantity})
    save_history()
    
    return redirect(url_for('index'))

@app.route('/balance', methods=['POST'])
def balance_change():
    operation = request.form['balance-type']
    value = float(request.form['balance-value'])
    
    global account_balance

    if value < 0:
        return "Invalid value", 400

    if operation == 'add':
        account_balance += value
    elif operation == 'subtract':
        if account_balance < value:
            return "Insufficient balance", 400
        account_balance -= value
    else:
        return "Invalid operation", 400

    history.append({'type': 'balance_change', 'operation': operation, 'value': value})
    save_history()
    
    return redirect(url_for('index'))

@app.route('/history/', defaults={'line_from': None, 'line_to': None})
@app.route('/history/<int:line_from>/<int:line_to>/')
def history_page(line_from, line_to):
    if line_from is None or line_to is None:
        return render_template('history.html', history=history)
    else:
        return render_template('history.html', history=history[line_from:line_to])

if __name__ == "__main__":
    app.run(debug=True)
commands = ["balance", "sale", "purchase", "account", "list", "warehouse", "review", "end"]
goodbye = False
balance = 0
productList = [["banana", 10, 10], ["apple", 10, 5], ["orange", 10, 15]]
historyList = []

while goodbye != True:
    print("\nAvailable commands:")
    for x in commands:
        print("- " + x)
    print("Please enter the command:")
    inCommand = input()
    if inCommand == "balance":
        userinput = input("Add or substract from current balance:")
        try:
            int(userinput)
            balance += int(userinput)
            historyList.append(("Balance changed: ", balance))
        except(ValueError):
            print("Incorrect value")
    elif inCommand == "sale":
        print("Enter name of the item to be sold:")
        checkList = input()
        checker = False
        qChecker = False
        for index, product in enumerate(productList):
            if product[0] == checkList:
                checker = True
                counter = index
        if checker == False:
            print("No Item named {}".format(checkList))
        elif checker == True:
            print("How many would you like to sell:")
            checkQuantity = int(input())
            if checkQuantity <= productList[counter][1]:
                print("Items sold")
                productList[counter][1] -= checkQuantity
                balance += checkQuantity * productList[counter][2]
                historyList.append(("Sold: ", checkList, checkQuantity))
            else:
                print("Not enough items")

    elif inCommand == "purchase":
        print("Enter name of the item to be purchased:")
        checkList = input()
        for index, product in enumerate(productList):
            if product[0] == checkList:
                checker = True
                counter = index
        if checker == False:
            print("No Item named {}".format(checkList))
        elif checker == True:
            print("How many would you like to buy:")
            checkQuantity = int(input())
            if balance >= (productList[counter][2] * checkQuantity):
                print("Items bought")
                productList[counter][1] += checkQuantity
                balance -= productList[counter][2] * checkQuantity
                historyList.append(("Purchased: ", checkList, checkQuantity))
            else:
                print("Not enough funds")

    elif inCommand == "account":
        print("Current balance {}".format(balance))
    elif inCommand == "list":
        for x in productList:
            print("Item: {}".format(x[0]))
            print("Quantity: {}".format(x[1]))
            print("Cost: {}".format(x[2]))
    elif inCommand == "warehouse":
        print("Enter product name to be checked:")
        checkList = input()
        checker = False
        for x in productList:
            if checkList == x[0]:
                print("Item: {}".format(x[0]))
                print("Quantity: {}".format(x[1]))
                print("Cost: {}".format(x[2]))
                checker = True
        if checker == False:
            print("No Item named {}".format(checkList))

    elif inCommand == "review":
        goodValue = True
        if not historyList:
            print("No operations found")
        else:
            fromIndex = input("Enter 'from' value: ")
            try:
                fromIndex = int(fromIndex)
                if fromIndex < 0 or fromIndex > len(historyList):
                    raise ValueError
            except ValueError:
                print("Invalid 'from' number")
                goodValue = False
            toIndex = input("Enter 'to' value: ")
            if toIndex == "":
                toIndex = len(historyList)
                for x in range(0, toIndex):
                    print(historyList[x])
                goodValue = False
            else:
                try:
                    toIndex = int(toIndex)
                    if toIndex < fromIndex or toIndex >= len(historyList):
                        raise ValueError
                except ValueError:
                    print("Invalid 'to' number")
                    goodValue = False
            if goodValue == True:
                print("History with specified range:")
                for x in range(fromIndex, toIndex + 1):
                    print(historyList[x])

    elif inCommand == "end":
        goodbye = True
    else:
        print("Incorrect command")
import requests
import datetime
import json
import os

# Constants for the API
LATITUDE = 51.5074  # Example: London
LONGITUDE = -0.1278  # Example: London
TIMEZONE = "Europe/London"
API_URL = "https://api.open-meteo.com/v1/forecast"
CACHE_FILE = "weather_cache.json"

class WeatherForecast:
    def __init__(self, cache_file):
        self.cache_file = cache_file
        self.load_cache()

    def load_cache(self):
        if os.path.exists(self.cache_file):
            with open(self.cache_file, 'r') as file:
                self.cache = json.load(file)
        else:
            self.cache = {}

    def save_cache(self):
        with open(self.cache_file, 'w') as file:
            json.dump(self.cache, file)

    def __setitem__(self, date, value):
        self.cache[date] = value
        self.save_cache()

    def __getitem__(self, date):
        return self.cache.get(date, None)

    def __iter__(self):
        return iter(self.cache.keys())

    def items(self):
        for date, weather in self.cache.items():
            yield date, weather

    def get_weather(self, date):
        params = {
            "latitude": LATITUDE,
            "longitude": LONGITUDE,
            "daily": "precipitation_sum",
            "timezone": TIMEZONE,
            "start_date": date,
            "end_date": date
        }

        try:
            print(f"Requesting URL: {API_URL} with params: {params}")  # Print the URL and params for debugging
            response = requests.get(API_URL, params=params)
            response.raise_for_status()
            data = response.json()
            return data['daily']['precipitation_sum'][0]
        except requests.exceptions.RequestException as e:
            print(f"Error fetching weather data: {e}")
            return None
        except KeyError:
            print("Unexpected response format")
            return None

def main():
    weather_forecast = WeatherForecast(CACHE_FILE)
    user_input = input("Enter the date (YYYY-mm-dd) to check the weather or leave empty for tomorrow: ")

    if not user_input:
        date_to_check = (datetime.datetime.now() + datetime.timedelta(days=1)).strftime("%Y-%m-%d")
    else:
        try:
            date_to_check = datetime.datetime.strptime(user_input, "%Y-%m-%d").strftime("%Y-%m-%d")
        except ValueError:
            print("Invalid date format. Please enter the date in YYYY-mm-dd format.")
            return

    cached_result = weather_forecast[date_to_check]
    if cached_result is not None:
        print(f"Using cached result for {date_to_check}:")
        precipitation = cached_result
    else:
        precipitation = weather_forecast.get_weather(date_to_check)
        if precipitation is not None:
            weather_forecast[date_to_check] = precipitation

    if precipitation is not None:
        if precipitation > 0.0:
            print(f"It will rain with precipitation of {precipitation} mm.")
        elif precipitation == 0.0:
            print("It will not rain.")
        else:
            print("I don't know.")
    else:
        print("I don't know.")

if __name__ == "__main__":
    main()

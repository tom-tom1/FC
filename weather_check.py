import requests
import datetime
import json
import os

# Constants for the API
LATITUDE = 51.5074  # Example: London
LONGITUDE = -0.1278  # Example: London
TIMEZONE = "Europe/London"
API_URL = "https://api.open-meteo.com/v1/forecast"

CACHE_FILE = "weather_cache.json"

def get_weather(date):
    params = {
        "latitude": LATITUDE,
        "longitude": LONGITUDE,
        "daily": "precipitation_sum",
        "timezone": TIMEZONE,
        "start_date": date,
        "end_date": date
    }

    try:
        response = requests.get(API_URL, params=params)
        response.raise_for_status()
        data = response.json()
        return data['daily']['precipitation_sum'][0]
    except requests.exceptions.RequestException as e:
        print(f"Error fetching weather data: {e}")
        return None
    except KeyError:
        print("Unexpected response format")
        return None

def save_to_cache(date, precipitation):
    if os.path.exists(CACHE_FILE):
        with open(CACHE_FILE, 'r') as file:
            cache = json.load(file)
    else:
        cache = {}

    cache[date] = precipitation

    with open(CACHE_FILE, 'w') as file:
        json.dump(cache, file)

def read_from_cache(date):
    if os.path.exists(CACHE_FILE):
        with open(CACHE_FILE, 'r') as file:
            cache = json.load(file)
        return cache.get(date)
    return None

def main():
    user_input = input("Enter the date (YYYY-mm-dd) to check the weather or leave empty for tomorrow: ")

    if not user_input:
        date_to_check = (datetime.datetime.now() + datetime.timedelta(days=1)).strftime("%Y-%m-%d")
    else:
        try:
            date_to_check = datetime.datetime.strptime(user_input, "%Y-%m-%d").strftime("%Y-%m-%d")
        except ValueError:
            print("Invalid date format. Please enter the date in YYYY-mm-dd format.")
            return

    cached_result = read_from_cache(date_to_check)
    if cached_result is not None:
        print(f"Using cached result for {date_to_check}:")
        precipitation = cached_result
    else:
        precipitation = get_weather(date_to_check)
        if precipitation is not None:
            save_to_cache(date_to_check, precipitation)

    if precipitation is not None:
        if precipitation > 0.0:
            print(f"It will rain with precipitation of {precipitation} mm.")
        elif precipitation == 0.0:
            print("It will not rain.")
        else:
            print("I don't know.")
    else:
        print("I don't know.")

if __name__ == "__main__":
    main()